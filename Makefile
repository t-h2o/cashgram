all:
	docker compose  up --build --remove-orphans -d
clean:
	docker compose down
fclean: clean
	docker container prune
	docker prune -a
re: fclean all

diagram:
	asciidoctor -r asciidoctor-diagram assets/README.adoc -o assets/index.html

doc: diagram
	asciidoctor README.adoc -o index.html
